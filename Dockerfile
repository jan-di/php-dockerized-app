FROM php:7.4-apache

# install packages via apt-get
RUN set -eux; \
	apt-get update; \
	apt-get install -y --no-install-recommends \
        unzip \
        git \
	; \
	rm -rf /var/lib/apt/lists/*

# install php extensions
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/
RUN set -eux; \
    install-php-extensions \
        gd \
        opcache \
    ; \
    rm /usr/local/bin/install-php-extensions

WORKDIR /var/www/html
RUN set -eux; \
    git clone --depth 1 https://gitlab.com/jan-di/php-skeleton /var/www/html

# install composer dependencies
COPY --from=composer /usr/bin/composer /usr/local/bin/
RUN set -eux; \
    composer install --no-dev --no-progress --classmap-authoritative; \
    rm /usr/local/bin/composer

# change apache config
RUN set -eux; \
    a2enmod rewrite; \
    sed -ri -e 's!/var/www/html!/var/www/html/public!g' /etc/apache2/sites-available/*.conf


COPY php.ini $PHP_INI_DIR/conf.d/custom.ini